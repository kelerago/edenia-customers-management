<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get("paises", 'utilidadesController@paises');
Route::resource('clientes', 'ClientesController', ['except' => ['create', 'edit', 'show']]);

Route::get('cantidad-clientes', 'utilidadesController@cantidadClientes');
Route::get('cantidad-paises', 'utilidadesController@cantidadPaises');
Route::get('clientes-pais', 'utilidadesController@clientesPorPaises');