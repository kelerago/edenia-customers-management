window.Vue = require('vue');
window.axios = require('axios');
window.Swal = require('sweetalert2');
window.Chart = require('chart.js');

Vue.component('estadisticas', require('./components/estadisticas.vue').default);
Vue.component('clientes', require('./components/clientes.vue').default);


const app = new Vue({
    el: '#app',
    data:{
        menu:0,
    },
    methods: {
        changePage(menu){
            this.menu = menu;
        }
    },
});
