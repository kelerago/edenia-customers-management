<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientesController extends Controller
{
    /**
     * Listado de Clientes
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::Table('clientes')
                    ->get();

        $response = [
            'code'      => 200,
            'clientes'  => $clientes,
            'status'    => 'success'
        ];
        
        return response()->json($response, $response['code']);
    }

    /**
     * Almacenamiento de Clientes
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Recepción de datos
        $data = $request->all();

        // Validación de datos
        $validation = \Validator::make($data, [
            'nombre_comercial'  => 'required',
            'razon_social'      => 'required',
            'nit'               => 'required|max:8',
            'email'             => 'email',
            'latitud'           => 'required',
            'longitud'          => 'required',
            'id_pais'           => 'exists:paises,id_pais'
        ]);

        if(!$validation->fails()){
            
            // Evitar modificaciones no deseadas
            unset($data['id_cliente']);
            
            // Inserción del cliente
            DB::Table('clientes')
            ->insert($data);

            $response = [
                'code'      => 201,
                'status'    => 'success'
            ];   

        } else{
            // Errores de Validación

            $response = [
                'code'      => 400,
                'errors'    => $validation->errors(),
                'status'    => 'error'
            ];
        }     
        
        // Respuesta
        return response()->json($response, $response['code']);
    }   

    /**
     * Actualización de Clientes
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Consulta de Cliente
        $cliente = DB::Table('clientes')
                   ->where('id_cliente', $id)
                   ->first();
        
        // Verificación de existencia de cliente
        if(is_object($cliente)){

            // Validación de información
            $data = $request->all();

            $validation = \Validator::make($data, [
                'nombre_comercial'  => 'required',
                'razon_social'      => 'required',
                'nit'               => 'required|max:8',
                'email'             => 'email',
                'id_pais'           => 'exists:paises,id_pais'
            ]);
            
            // Información correcta
            if(!$validation->fails()){

                // Evitar modificación de campos no deseados
                unset($data['id_cliente']);
                unset($data['latitud']);
                unset($data['longitud']);

                // Actualización de cliente
                DB::Table('clientes')
                ->where('id_cliente', $id)
                ->update($data);

                $response = [
                    'code'      => 200,
                    'status'    => 'success'
                ];
            
            // Información incorrecta    
            } else{

                $response = [
                    'code'      => 400,
                    'errors'    => $validation->errors(),
                    'status'    => 'error'
                ];
            }

        } else{

            $response = [
                'code'      => 404,
                'status'    => 'error'
            ];
        }
        
        return response()->json($response, $response['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Consulta de Cliente
        $cliente = DB::Table('clientes')
                   ->where('id_cliente', $id)
                   ->first();
        
        // Verificación de existencia de cliente
        if(is_object($cliente)){

            DB::Table('clientes')
            ->where('id_cliente', $id)
            ->delete();

            $response = [
                'code'      => 200,
                'status'    => 'success'
            ];
            
        } else {

            $response = [
                'code'      => 404,
                'status'    => 'error'
            ];
        }

        return response()->json($response, $response['code']);
    }
}
