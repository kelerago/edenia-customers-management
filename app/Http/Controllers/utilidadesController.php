<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class utilidadesController extends Controller
{

    /**
     * Listado de Paises
     * @return \Illuminate\Http\Response
     */

    public function paises()
    {

        $paises = DB::Table('paises')
                  ->get();

        $response = [
            'code'     => 200,
            'paises'   => $paises,
            'status'   => 'success' 
        ];   
        
        return response()->json($response);
    }

    public function cantidadClientes(){

        $clientes = DB::Table('clientes')
                    ->selectRaw('count(*) as cantidad')
                    ->first();

        $response = [
            'code'      => 200,
            'cantidad'  => $clientes->cantidad,
            'status'    => 'success'
        ];

        return response()->json($response, $response['code']);
    }

    public function cantidadPaises(){

        $paises = DB::Table('paises')
                  ->selectRaw('count(*) as cantidad')
                  ->first();

        $response = [
            'code'      => 200,
            'cantidad'  => $paises->cantidad,
            'status'    => 'success'
        ];
        
        return response()->json($response);
    }

    public function clientesPorPaises(){

        $paises = DB::Table('paises')
                  ->get();

        $data = [
            'paises'        => [],
            'cantidades'    => []
        ];          

        foreach($paises as $pais){

            $clientes = DB::Table('clientes')
                                ->selectRaw('count(*) as cantidad')
                                ->where('id_pais', $pais->id_pais)
                                ->first();

            if($clientes->cantidad > 0){

                $data['paises'][]   = $pais->descripcion;
                $data['cantidades'][] = $clientes->cantidad;
            }                               
        }          

        $response = [
            'code'          => 200,
            'clientes_pais' => $data,
            'status'        => 'success'
        ];

        return response()->json($response);
    }
}